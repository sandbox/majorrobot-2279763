<?php
/**
 * @file
 * Set up Mongor admin interface.
 */


/**
 * Mongor admin form.
 */
function mongor_admin_form($form_state) {
  $form = array();
  $views = views_get_views_as_options(FALSE, 'all', NULL, TRUE);

  $form['#theme'] = 'mongor_admin_form';

  $form['create'] = array(
    '#type'     => 'fieldset',
    '#title'    => t('Create a Mongo Relationship'),
    '#description'  => t("A Mongo Relationship pipes a source view's results into a second view's contextual filter, producing a relationship valid in specific contexts."),
    '#collapsible'  => TRUE,
    '#collapsed'  => FALSE,
  );

  $form['create']['title'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Administrative Title:'),
    '#description'    => t('Add a title to make it easier to find your Relationship later.'),
    '#default_value'  => '',
    '#size'       => 32,
  );

  $form['create']['src_view'] = array(
    '#type'       => 'select',
    '#title'      => t('Source View:'),
    '#default_value'  => 0,
    '#options'      => $views,
  );

  $form['create']['target_view'] = array(
    '#type'       => 'select',
    '#title'      => t('Target View:'),
    '#default_value'  => 0,
    '#options'      => $views,
  );

  $form['create']['context'] = array(
    '#type'       => 'select',
    '#title'      => t('Context:'),
    '#description'    => t("This is the context that will generate a 'seed' argument for the source view."),
    '#default_value'  => 0,
    '#options'      => array(
      'current_node_page' => t('Current Node Page'),
      'current_user'    => t('Current User'),
      'current_term'    => t('Current Taxonomy Term'),
    ),
  );

  $form['create']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Relationship'),
  );

  return $form;
}


/**
 * Mongor admin form submit handler.
 */
function mongor_admin_form_submit(&$form, &$form_state) {

  // Process form data, prepare for db.
  $vals = $form_state["values"];
  $src_vals = explode(":", $vals["src_view"]);
  $target_vals = explode(":", $vals["target_view"]);

  $insert = array(
    "src_view"      => $src_vals[0],
    "src_display"     => $src_vals[1],
    "target_view"   => $target_vals[0],
    "target_display"  => $target_vals[1],
    "context"     => $vals["context"],
    "title"       => $vals["title"],
  );

  $mongor_id = db_insert("mongor")
    ->fields($insert)
    ->execute();

  $msg = is_numeric($mongor_id) ? "Mongo relationship successfully created." : "There was a problem creating the relationship.";

  drupal_set_message($msg);
}

/**
 * Delete a Mongor Relationship from the db.
 */
function mongor_delete($mongor_id) {
  $num_deleted = db_delete("mongor")
    ->condition("mongor_id", $mongor_id)
    ->execute();

  $msg = $num_deleted > 0 ? "Relationship successfully deleted." : "There was a problem deleting the relationship.";

  drupal_set_message($msg);
  drupal_goto('admin/structure/mongor');
}


/**
 * Theme function for the mongor admin form.
 */
function theme_mongor_admin_form(&$vars) {
  $output = "";
  $form = $vars['form'];

  $output .= drupal_render_children($form);
  $output .= theme('table', mongor_get_table());

  return $output;
}

/**
 * Return table of previously created mongor relationships.
 */
function mongor_get_table() {
  $table = array();
  $table['header'] = array("Relationship", "Source View", "Source Display", "Target View", "Target Display", "Context", "Delete");

  $view_path = 'admin/structure/views/view/';

  $result = db_select('mongor', 'm')
    ->fields('m', array('mongor_id', 'title', 'src_view', 'src_display', 'target_view', 'target_display', 'context'))
    ->execute();

  foreach ($result as $record) {
    $table['rows'][] = array(
      '0' => $record->title,
      '1' => l($record->src_view, $view_path . $record->src_view . '/edit/' . $record->src_display),
      '2' => $record->src_display,
      '3' => l($record->target_view, $view_path . $record->target_view . '/edit/' . $record->target_display),
      '4' => $record->target_display,
      '5' => $record->context,
      '6' => l(t('Delete'), 'admin/structure/mongor/delete/' . $record->mongor_id),
    );
  }

  return $table;
}
